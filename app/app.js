var app = angular.module("convertUtcModule", ["validationsModule"]);
app.controller("convertUtcController", ["$scope", "$http", "validations", function(scope, http, validations){

    /* VARIABLES SCOPE */
    scope.timeZone = {};
    scope.btnConvertUTC = false;
    scope.timeUTC = {};
    /* FIN */

    /* VALIDACIONES */
    scope.validations = validations;
    /* FIN */

    /* METODO QUE CONSUME EL API REST PARA CONVERTIR LA HORA A UTC */
    scope.handleConvertUTC = function(){
        var data = {    zone: scope.timeZone.zone,
                        time: scope.timeZone.time
                    };
        scope.btnConvertUTC = true;
        http.post("https://convert-utc.herokuapp.com/api/formato-utc",data)
            .then(function (response){
                console.log(response);
                scope.timeUTC = response.data.response;
                scope.btnConvertUTC = false;
            },function (error){
                console.log(err);
                scope.btnConvertUTC = false;
            });
    };
    /* FIN */

    /* LIMPIAR FORMULARIO */
    scope.reset = function(form) {
        scope.timeZone = {};
        scope.timeUTC = {};
        if (form) {
          form.$setPristine();
          form.$setUntouched();
        }
    };
    /* FIN */

}]);