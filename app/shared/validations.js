var app = angular.module("validationsModule", []);
app.factory('validations', function() {
    var validationSvc = {};

    /* VALIDACION PARA EL CAMPO HORA */
    validationSvc.onlyNumberAndSimbol = function (e) {
        let key = e.keyCode || e.which;
        let tecla = String.fromCharCode(key).toLowerCase();
        let letras = "0123456789:";

        if(letras.indexOf(tecla) == -1 && key != 8)
            e.preventDefault();
    }
    /* FIN */

    /* VALIDACION PARA EL CAMPO ZONE */
    validationSvc.onlyNumberAndSimbols = function (e) {
        let key = e.keyCode || e.which;
        let tecla = String.fromCharCode(key).toLowerCase();
        let letras = "0123456789-+";

        if(letras.indexOf(tecla) == -1 && key != 8)
            e.preventDefault();
    }
    /* FIN */

    return  validationSvc;
});